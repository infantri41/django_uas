from django.db import models
from datetime import date
from django.utils import timezone

# Create your models here.
class Pengguna(models.Model):
	LEVEL_USER = (
		('admin','admin'),
		('staff','staff'),
	)

	JENIS_KLMN = (
		('WA','Wanita'),
		('PR','Pria'),
	)
	id_pengguna = models.CharField('ID User', max_length=12, null=False)
	nama = models.CharField('Nama User', max_length=50, null=False)
	tempat_lahir = models.CharField('Tempat Lahir (Kota, Provinsi)', max_length=50, null=False)
	jenis_kelamin = models.CharField('Jenis_Kelamin', max_length=2, choices=JENIS_KLMN, default='')
	username = models.CharField('Username', max_length=25, null=False)
	password = models.CharField('Password', max_length=25, null=False)
	level = models.CharField('Level Pengguna', max_length=5, choices=LEVEL_USER)

	class Meta:
		ordering = ['id_pengguna']

	def __str__(self):
		return self.nama
			
