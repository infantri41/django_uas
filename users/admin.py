from django.contrib import admin
from .models import Pengguna

# Register your models here.
@admin.register(Pengguna)
class Penggunaan(admin.ModelAdmin):
	list_display = ['nama','level','jenis_kelamin']
	list_filter = ['level','jenis_kelamin']
	search_fields = ['nama','id_pengguna']
