from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Obat
 
@admin.register(Obat)
class ObatAdmin(ImportExportModelAdmin):
    list_display = ("nama_obat", "jumlah")
    pass
