from django.db import models
from datetime import date
from django.utils import timezone

# Create your models here.
class Obat(models.Model):
	
	id_obat = models.CharField('ID Obat', max_length=12, null=False)
	nama_obat = models.CharField('Nama Obat', max_length=25, null=False)
	jumlah = models.IntegerField('Jumlah Obat', max_length=5, null=False)
	# tgl_input = models.DateTimeField('Tanggal Input',default=datetime.now)

	class Meta:
		ordering = ['id_obat']

	def __str__(self):
		return self.nama_obat
			
